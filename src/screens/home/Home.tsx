import { useRef, useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import {
  Alert,
  FlatList,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { Header } from '../../components/Header';
import { ListEmpty } from '../../components/ListEmpty';
import { Task } from '../../components/Task';

import plusImg from '../../assets/plus.png';

import { styles } from './styles';

type TTaskProp = {
  text: string;
  done: boolean;
};

export function Home() {
  const [inputText, setInputText] = useState('');
  const [tasks, setTasks] = useState<TTaskProp[]>([]);
  const inputRef = useRef<TextInput>(null);

  const tasksDone = tasks.filter((task) => task.done);

  function clearInput() {
    setInputText('');
    inputRef.current?.blur();
  }

  function handleAddTask() {
    if (!inputText.trim()) {
      return Alert.alert('Adicionar tarefa', 'Digite o nome da tarefa.');
    }
    if (tasks.find((task) => task.text === inputText)) {
      return Alert.alert('Adicionar tarefa', 'Essa tarefa já foi adicionada.');
    }
    setTasks([...tasks, { text: inputText, done: false }]);
    clearInput();
  }

  function handleTaskCheck(taskName: string) {
    const checkTask = tasks
      .map((task) =>
        task.text === taskName ? { ...task, done: !task.done } : task
      )
      .sort((taskA, taskB) => (taskA.done > taskB.done ? 1 : -1));

    setTasks(checkTask);
  }

  function handleRemoveTask(taskName: string) {
    const filteredTasks = tasks.filter((task) => task.text !== taskName);

    setTasks(filteredTasks);
  }

  return (
    <SafeAreaView style={styles.container}>
      <Header />

      <View style={styles.inputWrapper}>
        <TextInput
          ref={inputRef}
          style={styles.input}
          placeholder='Adicione uma nova tarefa'
          placeholderTextColor='#808080'
          onChangeText={setInputText}
          value={inputText}
          onSubmitEditing={handleAddTask}
          returnKeyType='done'
        />
        <TouchableOpacity style={styles.button} onPress={handleAddTask}>
          <Image source={plusImg} />
        </TouchableOpacity>
      </View>

      <View style={styles.taskInfo}>
        <View style={styles.tasksCreatedWrapper}>
          <Text style={[styles.taskInfoTitle, styles.tasksCreated]}>
            Criadas
          </Text>
          <Text style={styles.taskInfoDisplay}>{tasks.length}</Text>
        </View>
        <View style={styles.tasksCreatedWrapper}>
          <Text style={[styles.taskInfoTitle, styles.tasksDone]}>
            Concluídas
          </Text>
          <Text style={styles.taskInfoDisplay}>{tasksDone.length}</Text>
        </View>
      </View>

      <FlatList
        data={tasks}
        keyExtractor={(task) => task.text}
        renderItem={({ item }) => (
          <Task
            text={item.text}
            isChecked={item.done}
            onCheck={() => handleTaskCheck(item.text)}
            onRemove={() => handleRemoveTask(item.text)}
          />
        )}
        ListEmptyComponent={<ListEmpty />}
        contentContainerStyle={{ paddingBottom: 80 }}
      />
    </SafeAreaView>
  );
}
