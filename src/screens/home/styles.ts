import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1A1A',
  },
  inputWrapper: {
    paddingLeft: 24,
    paddingRight: 24,
    top: -32,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    color: '#F2F2F2',
    fontSize: 16,
    lineHeight: 20,
    backgroundColor: '#262626',
    padding: 16,
    borderRadius: 6,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#0D0D0D',
    flex: 1,
  },
  button: {
    backgroundColor: '#1E6F9F',
    width: 56,
    height: 56,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 4,
  },
  taskInfo: {
    paddingLeft: 24,
    paddingRight: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  tasksCreatedWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  taskInfoTitle: {
    fontSize: 14,
    fontWeight: '700',
  },
  taskInfoDisplay: {
    backgroundColor: '#333333',
    fontSize: 12,
    fontWeight: '700',
    marginLeft: 8,
    paddingVertical: 2,
    paddingHorizontal: 8,
    borderRadius: 999,
    color: '#D9D9D9',
  },
  tasksCreated: {
    color: '#4EA8DE',
  },

  tasksDone: {
    color: '#8284FA',
  },
});
