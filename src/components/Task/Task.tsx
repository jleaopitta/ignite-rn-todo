import { Image, Text, TouchableOpacity, View } from 'react-native';

import trashImg from '../../assets/trash.png';
import checkImg from '../../assets/check.png';
import { styles } from './styles';

type TTaskProps = {
  text: string;
  isChecked: boolean;
  onRemove: () => void;
  onCheck: () => void;
};

export function Task({ text, isChecked, onCheck, onRemove }: TTaskProps) {
  return (
    <View style={[styles.container, isChecked && styles.containerChecked]}>
      <TouchableOpacity
        style={[
          styles.radio,
          isChecked ? styles.radioChecked : styles.radioUnchecked,
        ]}
        onPress={onCheck}
      >
        {isChecked && <Image source={checkImg} />}
      </TouchableOpacity>
      <Text style={[styles.text, isChecked && styles.textChecked]}>{text}</Text>
      <TouchableOpacity onPress={onRemove}>
        <Image source={trashImg} />
      </TouchableOpacity>
    </View>
  );
}
