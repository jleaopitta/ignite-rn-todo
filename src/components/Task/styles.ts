import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#333333',
    flexDirection: 'row',
    marginLeft: 24,
    marginRight: 24,
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#333333',
    borderRadius: 8,
    paddingVertical: 12,
    paddingRight: 8,
    paddingLeft: 12,
    marginBottom: 8,
  },
  containerChecked: {
    backgroundColor: '#262626',
  },
  radio: {
    height: 24,
    width: 24,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 999,
  },
  radioChecked: {
    backgroundColor: '#5E60CE',
    borderWidth: 0,
  },
  radioUnchecked: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#4EA8DE',
    backgroundColor: 'transparent',
  },
  text: {
    marginLeft: 8,
    marginRight: 8,
    color: '#F2F2F2',
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '400',
    flex: 1,
  },
  textChecked: {
    textDecorationLine: 'line-through',
    color: '#808080',
  },
});
