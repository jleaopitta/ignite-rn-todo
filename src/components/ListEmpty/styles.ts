import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 48,
    paddingHorizontal: 20,
    borderTopWidth: 1,
    borderStyle: 'solid',
    borderTopColor: '#333',
  },
  text: {
    fontSize: 14,
    fontWeight: '700',
    lineHeight: 20,
    color: '#808080',
    marginTop: 16,
  },
});
