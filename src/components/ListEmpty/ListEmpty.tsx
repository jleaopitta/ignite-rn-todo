import { Image, Text, View } from 'react-native';

import clipboard from '../../assets/Clipboard.png';
import { styles } from './styles';

export function ListEmpty() {
  return (
    <View style={styles.container}>
      <Image source={clipboard} />
      <Text style={styles.text}>
        Você ainda não tem tarefas cadastradas. Crie tarefas e organize seus
        itens a fazer
      </Text>
    </View>
  );
}
